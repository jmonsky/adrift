use std::sync::mpsc::{channel, Receiver, Sender};
use bevy::prelude::*;
use bevy::utils::Instant;
use bevy_steamworks::*;
use bevy_inspector_egui::quick::ResourceInspectorPlugin;


struct SteamLobbyManager {
	lobby_rx: Receiver<Vec<LobbyId>>,
	lobby_tx: Sender<Vec<LobbyId>>,

	joining_lobby_rx: Receiver<LobbyId>,
	joining_lobby_tx: Sender<LobbyId>,
}

impl Default for SteamLobbyManager {
	fn default() -> Self {
		let (ltx, lrx) = channel();
		let (jltx, jlrx) = channel();
		return Self {
			lobby_rx: lrx,
			lobby_tx: ltx,

			joining_lobby_rx: jlrx,
			joining_lobby_tx: jltx,
		};
	}
}

#[derive(Resource)]
struct FriendsList {
	friends_online: Vec<(String, SteamId)>,
	friends_in_game: Vec<(String, SteamId)>,
	friends_all: Vec<(String, SteamId)>,

	last_updated: Instant,
}

impl Default for FriendsList {
	fn default() -> Self {
		return Self {
			friends_online: Vec::new(),
			friends_in_game: Vec::new(),
			friends_all: Vec::new(),

			last_updated: Instant::now(),
		};
	}
}

#[derive(Resource, Reflect)]
#[reflect(Resource)]
struct LobbyRequests {
	current_lobby: Option<u64>,
	lobby_members: Vec<String>,
	create_lobby: bool,

	auto_refresh: bool,
	manual_refresh: bool,
}

impl Default for LobbyRequests {
	fn default() -> Self {
		return Self {
			lobby_members: Vec::new(),
			current_lobby: None,
			create_lobby: false,
			auto_refresh: false,
			manual_refresh: false,
		};
	}
}

#[derive(Resource)]
struct LobbyList {
	current_lobby: Option<LobbyId>,
	lobbies_available: Vec<(LobbyId, bool)>, // bool is true when friend is in lobby


	request_join_lobby: Option<LobbyId>,

	last_updated: Instant,
}

impl Default for LobbyList {
	fn default() -> Self {
		return Self {
			current_lobby: None,
			lobbies_available: Vec::new(),

			request_join_lobby: None,

			last_updated: Instant::now(),
		};
	}
}

fn my_init_system(world: &mut World) {
	world.insert_non_send_resource(SteamLobbyManager::default());
	world.insert_resource(FriendsList::default());
	world.insert_resource(LobbyList::default());
	world.insert_resource(LobbyRequests::default());
}

fn steam_init_system(steam_client: Res<Client>, mut friends_list: ResMut<FriendsList>) {
	println!("steam_client.friends().name() = {}", steam_client.friends().name());

	let (_, ticket) = steam_client.user().authentication_session_ticket();
	if let Err(e) = steam_client.user().begin_authentication_session(steam_client.user().steam_id(), &ticket) {
		println!("Failed to begin an authentication session: {:?}", e);
	} else {
		println!("Authentication session started successfully!");
	}

	for friend in steam_client.friends().get_friends(FriendFlags::IMMEDIATE) {
		friends_list.friends_all.push((friend.name(), friend.id()));
	}
}

fn steam_update_system(
	steam_client: Res<Client>,
	lobby_state: NonSend<SteamLobbyManager>,
	mut lobby_requests: ResMut<LobbyRequests>,
	mut friends_list: ResMut<FriendsList>,
	mut lobby_list: ResMut<LobbyList>,
) {
	while let Ok(lobbies) = lobby_state.lobby_rx.try_recv() {
		lobby_list.lobbies_available.clear();
		lobby_list.last_updated = Instant::now();
		'lobby_loop: for lobby in lobbies {
			let users = steam_client.matchmaking().lobby_members(lobby);
			for user in users {
				for (_, id) in &friends_list.friends_all {
					if user == *id {
						println!("Friend lobby detected with lobby {:?} and user {:?}", lobby, user);
						lobby_list.lobbies_available.push((lobby, true));
						continue 'lobby_loop;
					}
				}
			}
			lobby_list.lobbies_available.push((lobby, false));
		}
	}

	while let Ok(new_lobby) = lobby_state.joining_lobby_rx.try_recv() {
		if let Some(current_lobby) = lobby_list.current_lobby {
			println!("Leaving lobby {:?} to join lobby {:?}", current_lobby, new_lobby);
			steam_client.matchmaking().leave_lobby(current_lobby);
			lobby_list.current_lobby = None;
		}
		lobby_list.current_lobby = Some(new_lobby);
	}

	if lobby_requests.create_lobby {
		lobby_requests.create_lobby = false;
		let lobby_sender = lobby_state.joining_lobby_tx.clone();
		steam_client.matchmaking().create_lobby(LobbyType::FriendsOnly, 4, move |id| {
			if let Ok(id) = id {
				let _ = lobby_sender.send(id);
			}
		});
	}

	if let Some(new_lobby) = lobby_list.request_join_lobby {
		let lobby_sender = lobby_state.joining_lobby_tx.clone();
		steam_client.matchmaking().join_lobby(new_lobby, move |id| {
			if let Ok(id) = id {
				let _ = lobby_sender.send(id);
			}
		});
		lobby_list.request_join_lobby = None;
	}

	if lobby_requests.auto_refresh || lobby_requests.manual_refresh {
		if lobby_list.last_updated.elapsed().as_secs_f64() > 5.0 || lobby_requests.manual_refresh {
			let send_lobby = lobby_state.lobby_tx.clone();
			steam_client.matchmaking().request_lobby_list(move |lobbies| {
				let _ = send_lobby.send(lobbies.unwrap());
			});
			lobby_list.last_updated = Instant::now();
			lobby_requests.manual_refresh = false;
		}
	}

	if friends_list.last_updated.elapsed().as_secs_f64() > 5.0 {
		let current_app = steam_client.utils().app_id();
		friends_list.friends_online.clear();
		friends_list.friends_in_game.clear();
		for friend in steam_client.friends().get_friends(FriendFlags::IMMEDIATE).iter().filter(|f| f.state() == FriendState::Online) {
			if let Some(current_game) = friend.game_played() {
				if current_game.game.app_id() == current_app {
					friends_list.friends_in_game.push((friend.name(), friend.id()));
					continue;
				}
			}
			friends_list.friends_online.push((friend.name(), friend.id()));
		}
		friends_list.last_updated = Instant::now();
	}

	lobby_requests.current_lobby = lobby_list.current_lobby.and_then(|v| Some(v.raw()));
}

fn input_system(
	keyboard_input: Res<bevy::input::Input<KeyCode>>,
	steam_client: Res<Client>,
	friends_list: Res<FriendsList>,
	mut lobby_list: ResMut<LobbyList>,
	mut lobby_requests: ResMut<LobbyRequests>,
) {
	if keyboard_input.just_pressed(KeyCode::I) {
		if let Some(current_lobby) = lobby_list.current_lobby {
			println!("Opening invite screen for lobby {:?}", current_lobby);
			steam_client.friends().activate_invite_dialog(current_lobby);
		} else {
			println!("Not in a lobby to invite people into!")
		}
	}


	if keyboard_input.just_pressed(KeyCode::F) {
		println!("Friends In-Game:");
		for (name, id) in &friends_list.friends_in_game {
			if let Some(game) = steam_client.friends().get_friend(*id).game_played() {
				println!("{} with id: {:?} in lobby {:?}", name, id, game.lobby);
			} else {
				println!("{} with id: {:?}", name, id);
			}
		}
		println!("Friends Online:");
		for (name, id) in &friends_list.friends_online {
			println!("{} with id: {:?}", name, id);
		}
	}

	if keyboard_input.just_pressed(KeyCode::U) {
		let mut joined = false;
		for (name, id) in &friends_list.friends_in_game {
			if let Some(game) = steam_client.friends().get_friend(*id).game_played() {
				if game.lobby.raw() != 0 {
					print!("Attempting to join {} in lobby {:?}", name, game.lobby);
					lobby_list.request_join_lobby = Some(game.lobby);
					joined = true;
					break;
				}
			}
		}
		if !joined {
			println!("Unable to join friends lobby!");
		}
	}

	if keyboard_input.just_pressed(KeyCode::L) {
		println!("Availabe Lobbies:");
		for (lobby, has_friends) in &lobby_list.lobbies_available {
			let lobby_name = steam_client.matchmaking().lobby_data(*lobby, "name").unwrap_or("Unknown Name").to_string();
			if *has_friends {
				println!("{}: {:?} (Friends in lobby)", lobby_name, lobby);
			} else {
				println!("{}: {:?}", lobby_name, lobby);
			}
		}
	}

	if keyboard_input.just_pressed(KeyCode::Y) {
		if let Some(current_lobby) = lobby_list.current_lobby {
			if steam_client.matchmaking().lobby_owner(current_lobby) == steam_client.user().steam_id() {
				println!("You own this lobby, setting data");
				steam_client.matchmaking().set_lobby_data(current_lobby, "name", format!("{}'s Lobby", steam_client.friends().name()).as_str());
				steam_client.matchmaking().set_lobby_data(current_lobby, "owner_name", steam_client.friends().name().as_str());
				steam_client.matchmaking().set_lobby_data(current_lobby, "game", "adrift");
			} else {
				println!("You are not the owner of this lobby!");
			}
		} else {
			println!("Not in a lobby to add data to!");
		}
	}

	if keyboard_input.just_pressed(KeyCode::N) {
		println!("Creating Lobby!");
		lobby_requests.create_lobby = true;
	}

	if keyboard_input.just_pressed(KeyCode::C) {
		lobby_requests.lobby_members.clear();
		if let Some(current_lobby) = lobby_list.current_lobby {
			println!("Currently in lobby: {:?}", current_lobby);
			let members = steam_client.matchmaking().lobby_members(current_lobby);
			println!("Members in current lobby");
			for member in members {
				let name = steam_client.friends().get_friend(member).name();
				lobby_requests.lobby_members.push(name.clone());
				println!("{} with id of {:?}", name, member);
			}
			println!("Data in current lobby");
			let count = steam_client.matchmaking().lobby_data_count(current_lobby);
			for idx in 0..count {
				if let Some((key, val)) = steam_client.matchmaking().lobby_data_by_index(current_lobby, idx) {
					println!("{} = {}", key, val);
				}
			}
		} else {
			println!("Currently not in a lobby");
		}
	}

	if keyboard_input.just_pressed(KeyCode::R) {
		println!("Refreshing Lobbies!");
		lobby_requests.manual_refresh = true;
	}

	if keyboard_input.just_pressed(KeyCode::Q) {
		if let Some(current_lobby) = lobby_list.current_lobby {
			println!("Leaving Lobby: {:?}", current_lobby);
			lobby_list.current_lobby = None;
			steam_client.matchmaking().leave_lobby(current_lobby);
		} else {
			println!("No lobby to leave!");
		}
	}

	if keyboard_input.just_pressed(KeyCode::J) {
		if lobby_list.lobbies_available.len() == 0 {
			println!("No lobbies available!");
		} else {
			let mut found = false;
			for (id, has_friends) in &lobby_list.lobbies_available {
				if *has_friends {
					println!("Requesting to join lobby {:?}", *id);
					lobby_list.request_join_lobby = Some(*id);
					found = true;
					break;
				}
			}
			if !found {
				println!("No friend's lobby to join");
			}
		}
	}

	if keyboard_input.just_pressed(KeyCode::D) {
		for (id, _) in &lobby_list.lobbies_available {
			println!("Lobby id: {:?}", id);
			if let Some(current_lobby) = lobby_list.current_lobby {
				if *id == current_lobby {
					println!("This is the current lobby!");
				}
			}
			let owner = steam_client.matchmaking().lobby_owner(*id);
			println!("- Owner: {:?}", owner);
			println!("- Members: ");
			for pid in steam_client.matchmaking().lobby_members(*id) {
				let name = steam_client.friends().get_friend(pid).name();
				println!("- - {} with id of {:?}", name, pid);
			}
			let count = steam_client.matchmaking().lobby_data_count(*id);
			println!("- Lobby Data:");
			for idx in 0..count {
				if let Some((key, val)) = steam_client.matchmaking().lobby_data_by_index(*id, idx) {
					println!(" - - {} = {}", key, val);
				}
			}
		}
	}
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Default, States, Debug)]
enum TestState {
	#[default]
	MainMenu,
	Paused,
}

fn enter_menu_system() {
	println!("MAIN MENU!!!");
}

fn main() {
	// Use the demo Steam AppId for SpaceWar
	App::new()
		.add_plugins(DefaultPlugins)
		.add_plugins(SteamworksPlugin::new(AppId(480)))
		.add_plugins(ResourceInspectorPlugin::<LobbyRequests>::default())
		.add_state::<TestState>()
		.add_systems(Startup, my_init_system)
		.add_systems(Startup, steam_init_system)
		.add_systems(Update, steam_update_system)
		.add_systems(Update, input_system)
		.add_systems(OnEnter::<TestState>(TestState::MainMenu), enter_menu_system)
		.run()
}